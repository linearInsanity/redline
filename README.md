Redline
=======

###Planned Features

```
-Nano Like Modifier Keys
-Multiple User Accounts
-Tabbed Subreddits
-Full Color and Comment System
-Mail and User View Screens
-Open Content using External Programs
-Filters and Blacklists
-Alert on Update
-Extendable Application API
```

###Dependencies
#####PRAW (Python Reddit API Wrapper) 
To install using pip, issue the following command:
```
$ pip install praw
```

Alternatively you can do it via easy_install:

```
$ easy_install praw
```
