import curses
import praw

def main(mainwin):
	#Initialize Colors
	curses.init_pair(1,curses.COLOR_RED, curses.COLOR_BLUE)
	curses.init_pair(2,curses.COLOR_BLACK, curses.COLOR_WHITE)
	
	#Initialize the Main Window
	mainwin.bkgd(' ', curses.color_pair(1))
	bounds = mainwin.getmaxyx()
	
	#Initialize the Logo
	logobounds = ((bounds[1] // 2) - 20)
	writetext(mainwin, r"", logobounds, 2, curses.A_BLINK)
	writetext(mainwin, r"", logobounds, 3, curses.A_BLINK)
	writetext(mainwin, r"", logobounds, 4, curses.A_BLINK)
	writetext(mainwin, r"", logobounds, 5, curses.A_BLINK)
	writetext(mainwin, r"", logobounds, 6, curses.A_BLINK)
	writetext(mainwin, r"", logobounds, 7, curses.A_BLINK)
                                                   
	#Initialize the Main Menu
	menubounds = [((bounds[1] // 2) - 20), ((bounds[0] // 2) - 5)]
	menu = createwindow(menubounds[0], menubounds[1], 10, 40)
	menu.bkgd(' ', curses.color_pair(2))
	menu.box()
	mbounds = menu.getmaxyx()
	#Draw Main Menu
	writetext(menu, "Login", 1, 1)
	writetext(menu, "Settings", 1, 3)
	writetext(menu, "Exit", 1, 5)
	writetext(menu, "W to Move, E to Select", ((mbounds[0] // 2)+4), 8, curses.A_STANDOUT)
	#Allow User to Choose
	selected = 0
	while 1:
		refresh(mainwin)
		refresh(menu)
		ichar = mainwin.getch()
		if ichar == ord('s'):
			selected += 1
		if ichar == ord('w'):
			selected -= 1
		if selected < 0:
			selected = 2
		if selected > 2:
			selected = 0
		#Highlight Options
		if selected == 0:
			writetext(menu, "Login", 1, 1, curses.A_STANDOUT)
			writetext(menu, "Settings", 1, 3)
			writetext(menu, "Exit", 1, 5)
			writetext(menu, "W to Move, E to Select", ((mbounds[1] // 2) - 22), 8, curses.A_STANDOUT)
		if selected == 1:
			writetext(menu, "Login", 1, 1)
			writetext(menu, "Settings", 1, 3, curses.A_STANDOUT)
			writetext(menu, "Exit", 1, 5)
			writetext(menu, "W to Move, E to Select", ((mbounds[1] // 2) - 22), 8, curses.A_STANDOUT)
		if selected == 2:
			writetext(menu, "Login", 1, 1)
			writetext(menu, "Settings", 1, 3)
			writetext(menu, "Exit", 1, 5, curses.A_STANDOUT)
			writetext(menu, "W to Move, E to Select", ((mbounds[1] // 2) - 22), 8, curses.A_STANDOUT)
		if (ichar == curses.KEY_ENTER):
			writetext(mainwin, "This is not a problem", ((mbounds[1] // 2) - 22), 2, curses.A_STANDOUT)
			break
	while 1:
		#Do New Window & Delete Menu
		pass
def createwindow(x, y, height, width):
	win = curses.newwin(height, width, y, x)
	return win

def writetext(window, string, x, y, form = curses.A_BOLD):
	window.addstr(y, x, string, form)

def move(window, x, y):
	window.move(y,x)
	refresh(window)	

def refresh(window):
	window.refresh()
	
def gettext(window,x,y,cols):
	window.getstr(x,y, 15)

if __name__ == "__main__":
	curses.wrapper(main)
